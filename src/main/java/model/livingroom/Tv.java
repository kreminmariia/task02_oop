package model.livingroom;

public class Tv extends LivingRoom {

    public Tv(String id, String name, String manufacturer, double price, String typeOfFurniture) {
        super(id, name, manufacturer, price, typeOfFurniture);
    }
}
