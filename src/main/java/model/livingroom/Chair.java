package model.livingroom;

public class Chair extends LivingRoom {
    private static String ID = "CH1995";

    public Chair(String name, String manufacturer, double price, String typeOfFurniture) {
        super(ID, name, manufacturer, price, typeOfFurniture);
    }

}
