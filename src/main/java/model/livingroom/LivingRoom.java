package model.livingroom;

import model.bases.Product;

public abstract class LivingRoom extends Product {
    private String typeOfFurniture;

    public LivingRoom(String id, String name, String manufacturer, double price, String typeOfFurniture) {
        super(id, name, manufacturer, price);
        this.typeOfFurniture = typeOfFurniture;
    }
}
