package model.bathroom;

import model.bases.Product;

public abstract class Bathroom extends Product {
    protected String material;

    public Bathroom(String id, String name, String manufacturer, double price, String material) {
        super(id, name, manufacturer, price);
        this.material = material;
    }
}
