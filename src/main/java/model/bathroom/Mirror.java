package model.bathroom;


public class Mirror extends Bathroom {

    public Mirror(String id, String name, String manufacturer, double price, String material) {
        super(id, name, manufacturer, price, material);
    }
}
