package model.bathroom;


public class Shower extends Bathroom {

    public Shower(String id, String name, String manufacturer, double price, String material) {
        super(id, name, manufacturer, price, material);
    }
}
