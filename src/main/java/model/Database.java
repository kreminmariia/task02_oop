package model;

import model.bases.Product;
import model.bathroom.Mirror;
import model.bathroom.Shower;
import model.kitchen.Microwave;
import model.kitchen.Table;
import model.livingroom.Chair;
import model.livingroom.Tv;

import java.util.ArrayList;

public class Database {
    private ArrayList<Product> itemList;

    public Database() {
        itemList = new ArrayList();
        // imitating real DB behavior
        fetchData();
    }

    public void addItemToWarehouse(Product product) {
        itemList.add(product);
    }

    public ArrayList<Product> getItemList() {
        return itemList;
    }

    private void fetchData() {
        addItemToWarehouse(new Mirror("M1", "Big mirror", "Ikea", 1341.3, "Wood and mirror"));
        addItemToWarehouse(new Mirror("M2", "Medium mirror", "Epicentr", 783, "Plastic and mirror"));
        addItemToWarehouse(new Mirror("M3", "Small mirror", "Ikea", 345.5, "Mirror"));
        addItemToWarehouse(new Shower("S2", "Medium shower", "Ikea", 12812.1, "Glass"));
        addItemToWarehouse(new Shower("S3", "Small shower", "Metro", 1410.3, "Glass"));
        addItemToWarehouse(new Shower("S1", "Big shower", "Metro", 15410.3, "Glass"));
        addItemToWarehouse(new Microwave("MW1", "Big microwave", "Delfa", 2410.3, "Metal and Plastic"));
        addItemToWarehouse(new Microwave("MW2", "Medium microwave", "Tefal", 1410.3, "Metal and Plastic"));
        addItemToWarehouse(new Microwave("MW3", "Small microwave", "Delfa", 610.3, "Metal and Plastic"));
        addItemToWarehouse(new Table("T1", "Wooden table", "IKEA", 6130.3, "Wood"));
        addItemToWarehouse(new Table("T2", "Table", "IKEA", 610.3, "Artificial wood"));
        addItemToWarehouse(new Tv("TV1", "TV", "Electron", 610.3, "Artificial wood"));
        addItemToWarehouse(new Tv("TV2", "TV", "Sony", 6010.3, "Artificial wood"));
        addItemToWarehouse(new Tv("TV3", "TV", "Saturn", 4520.3, "Artificial wood"));
        addItemToWarehouse(new Tv("TV4", "TV", "Bosh", 9853, "Artificial wood"));
        addItemToWarehouse(new Chair("Chair", "Ikea", 347.2, "Wood"));

    }
}
