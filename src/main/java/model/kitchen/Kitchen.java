package model.kitchen;

import model.bases.Product;

public abstract class Kitchen extends Product {
    String typeOfFurniture;

    public Kitchen(String id, String name, String manufacturer, double price, String typeOfFurniture) {
        super(id, name, manufacturer, price);
        this.typeOfFurniture = typeOfFurniture;
    }
}
