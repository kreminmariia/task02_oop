package model.kitchen;

public class Table extends Kitchen {

    public Table(String id, String name, String manufacturer, double price, String typeOfFurniture) {
        super(id, name, manufacturer, price, typeOfFurniture);
    }
}
