package model.kitchen;

public class Microwave extends Kitchen {

    public Microwave(String id, String name, String manufacturer, double price, String typeOfFurniture) {
        super(id, name, manufacturer, price, typeOfFurniture);
    }
}
