package model.bases;

public abstract class Product implements Comparable {
    protected String id;
    protected String name;
    protected String manufacturer;
    protected double price;

    public Product(String id, String name, String manufacturer, double price) {
        this.id = id;
        this.name = name;
        this.manufacturer = manufacturer;
        this.price = price;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", price=" + price +
                '}';
    }

    public int compareTo(Object other) {
        Product otherProduct = (Product) other;
        if (this.price == otherProduct.price) {
            return 0;
        } else if (this.price > otherProduct.price) {
            return 1;
        } else {
            return -1;
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
