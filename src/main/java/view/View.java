package view;

//import controller.InputHandler;

import controller.Controller;

import java.util.Scanner;

import static constants.StringConstants.*;

public class View {
    static Scanner sc = new Scanner(System.in);

    public static void printMessage(String message) {
        System.out.println(message);
    }

    public static void welcome() {
        printMessage(GREETING);
        scanCategoryChoice();
        printMessage(OPTIONCHOOSING);
        scanOperationChoice();
    }

    public static int reEnterToProgram() {
        int reEnterChoice = sc.nextInt();
        return reEnterChoice;
    }

    public static void scanCategoryChoice() {
        int listItem = sc.nextInt();
        try {
            Controller.selectCategory(listItem);
        } catch (ArrayIndexOutOfBoundsException e) {
            View.printMessage(INCORRECTVALUES);
            scanCategoryChoice();
        }
    }

    public static void scanOperationChoice() {
        int optionItem = sc.nextInt();
        try {
            Controller.selectOperation(optionItem);
        } catch (ArrayIndexOutOfBoundsException e) {
            View.printMessage(INCORRECTVALUES);
            scanOperationChoice();
        }
    }

    public static String getNameToSearchFor() {
        printMessage("Please type the name to look for (e.g. Small Mirror)");
        String name = sc.next();
        return name;
    }

}
