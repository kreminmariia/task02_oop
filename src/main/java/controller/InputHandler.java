package controller;

import constants.Categories;
import constants.Operations;

public class InputHandler {


    public String getCategory(int listItem) {
        return Categories.values()[listItem - 1].getCategoryName();
    }

    public Operations getOperation(int operationItem) {
        return Operations.values()[operationItem - 1];
    }
}

