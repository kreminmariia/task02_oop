package controller;

import constants.Operations;
import model.Database;
import model.bases.Product;
import view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static constants.StringConstants.*;


public class Controller {
    private static Database db = new Database();

    private static List<Product> listOFProducts;
    private static InputHandler inputHandler = new InputHandler();

    public static void selectCategory(int categoryIndex) {
        String requiredType = inputHandler.getCategory(categoryIndex);
        filterByType(requiredType);
    }

    public static void selectOperation(int operationIndex) {
        Operations operation = inputHandler.getOperation(operationIndex);
        List<Product> products = doOperation(operation);
        updateView(products);
        reask();
    }

    public static void filterByType(String type) {
        ArrayList<Product> filteredList = new ArrayList<>();
        for (Product p : db.getItemList()) {
            if (p.getClass().getSuperclass().getSimpleName().equalsIgnoreCase(type)) {
                filteredList.add(p);
            }
        }
        listOFProducts = filteredList;
    }

    private static List<Product> getListOFProducts() {
        return listOFProducts;
    }

    private static List<Product> sortByPrice() {
        Collections.sort(listOFProducts);
        return listOFProducts;
    }

    private static List<Product> searchByName(String name) {
        List<Product> products = new ArrayList<>();
        for (Product product : listOFProducts) {
            if (product.getName().toLowerCase().contains(name.toLowerCase())) {
                products.add(product);
            }
        }
        if (products.isEmpty()) {
            System.out.println("No product");
        }
        return products;
    }

    private static List<Product> doOperation(Operations operation) {
        List<Product> list = null;
        switch (operation) {
            case SORT:
                list = sortByPrice();
                break;
            case VIEW:
                list = getListOFProducts();
                break;
            case SEARCH:
                list = searchByName(View.getNameToSearchFor());
                break;

        }
        return list;
    }

    private static void updateView(List<Product> products) {
        for (Product product : products) {
            View.printMessage(product.toString());
        }
    }

    public static void reask() {
        View.printMessage(REASKING);
        int userChoice = View.reEnterToProgram();
        if (userChoice == 1) {
            View.welcome();
        } else if (userChoice == 2) {
            View.printMessage(BYEBYESAYING);
        } else {
            View.printMessage(INCORRECTVALUES);
            reask();
        }
    }

}
