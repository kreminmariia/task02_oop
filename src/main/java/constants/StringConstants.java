package constants;

public class StringConstants {
    public static String GREETING = "Welcome to our store. Please, choose the product's group:" +
            "\n1.Bathroom \n2.Living Room \n3.Kitchen";
    public static String OPTIONCHOOSING = "Please choose the option: \n1.View the list of products" +
            "\n2.Sort the list of products by price." +
            "\n3.Search product.";
    public static String REASKING = "\n\n\nStart from beginning \n1.Yes     2.No";
    public static String BYEBYESAYING = "Thanks. Have a great day!";
    public static String INCORRECTVALUES = "Please, type correct value";
}
