package constants;

public enum Categories {
    BATHROOM("Bathroom"), LIVINGROOM("LivingRoom"), KITCHEN("Kitchen");

    String categoryName;

    Categories(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }
}
